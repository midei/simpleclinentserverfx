package ru.kemsu.semestrwork.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public final class SimpleServer implements IServer {

    private final int port;

    private ServerSocket serverSocket = null;
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    private final Consumer<String> addMessage;

    private SimpleServer(Consumer<String> addMessage, int port) {
        this.addMessage = addMessage;
        this.port = port;
    }

    public static SimpleServer create(Consumer<String> addMessage, int port) {
        return new SimpleServer(addMessage, port);
    }

    public void start()   {
        try {
            serverSocket = new ServerSocket(port);
            addMessage.accept("Server started");
            while (!serverSocket.isClosed()) {
                Socket client = serverSocket.accept();
                executorService.submit(ClientTask.createTask(client));
            }
        } catch (SocketException ignored) {
        } catch (IOException e) {
            addMessage.accept("Error starting Server on " + port);
        }
    }

    public void stop() {
        try {
            executorService.shutdownNow();
            serverSocket.close();
            addMessage.accept("Server stopped");
        } catch (IOException e) {
            addMessage.accept("Error in server shutdown");
        }
    }
}
