package ru.kemsu.semestrwork;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class Application extends javafx.application.Application {

    private Controller controller;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 600);
        controller = fxmlLoader.getController();
        stage.setOnCloseRequest(this::handleClose);
        stage.setTitle("Simple client/server app");
        stage.setScene(scene);
        stage.show();
    }

    private void handleClose(WindowEvent event) {
        controller.closeClientAndServer();
        System.exit(0);
    }

    public static void main(String[] args) {
        launch();
    }
}