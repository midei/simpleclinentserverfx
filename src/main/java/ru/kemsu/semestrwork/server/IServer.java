package ru.kemsu.semestrwork.server;

import java.util.function.Consumer;

public interface IServer {
    void start();
    void stop();
}
