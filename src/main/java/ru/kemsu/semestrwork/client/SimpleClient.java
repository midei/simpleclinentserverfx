package ru.kemsu.semestrwork.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.function.Consumer;

public class SimpleClient implements IClient {
    private final Consumer<String> addMessage;

    private Socket socket;
    private DataOutputStream outputStream;
    private DataInputStream inputStream;

    public SimpleClient(Consumer<String> addMessage, String host, int port) {
        this.addMessage = addMessage;
        try {
            socket = new Socket(host, port);
            outputStream = new DataOutputStream(socket.getOutputStream());
            inputStream = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(String message) {
        try {
            outputStream.writeUTF(message);
            outputStream.flush();
            addMessage.accept("Client sent message " + message + " to server.");
            Thread.sleep(1000);
            if (inputStream.available() != 0) {
                addMessage.accept(inputStream.readUTF());
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        closeConnections(outputStream, inputStream, socket);
    }

    private void closeConnections(DataOutputStream outputStream, DataInputStream inputStream, Socket client) {
        try {
        outputStream.close();
        inputStream.close();
        client.close();
        } catch (IOException e) {
            addMessage.accept("Error in client shutdown");
        }
    }
}
