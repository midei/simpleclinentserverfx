module ru.kemsu.semestrwork {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;

    opens ru.kemsu.semestrwork to javafx.fxml;
    exports ru.kemsu.semestrwork;
}