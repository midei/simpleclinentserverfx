package ru.kemsu.semestrwork.client;

import java.util.function.Consumer;

public class ClientFactory {
    public static IClient createInstance(Consumer<String> addMessage, String host, int port) {return new SimpleClient(addMessage, host,  port);}
}
