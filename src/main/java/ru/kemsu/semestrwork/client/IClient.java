package ru.kemsu.semestrwork.client;

import java.io.IOException;

public interface IClient {
    void send(String message);
    void close();
}
