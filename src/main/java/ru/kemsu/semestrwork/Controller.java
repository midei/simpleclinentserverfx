package ru.kemsu.semestrwork;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ru.kemsu.semestrwork.client.ClientFactory;
import ru.kemsu.semestrwork.client.IClient;
import ru.kemsu.semestrwork.server.IServer;
import ru.kemsu.semestrwork.server.SimpleServer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Controller {
    private static final int SERVER_PORT = 7777;
    private static final String HOST = "localhost";

    @FXML
    private TextArea textArea;
    @FXML
    private TextField message;

    private IServer server = null;

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private IClient client = null;

    @FXML
    protected void onStartButtonClick() {
        if (server == null) {
            executor.submit(() -> {
                server = SimpleServer.create(this::addMessage, SERVER_PORT);
                server.start();
            });
        }
    }

    @FXML
    protected void onStopButtonClick() {
        if (server != null) {
            server.stop();
            server = null;
        }
    }

    public void closeClientAndServer() {
        onStopButtonClick();
        if (client != null) client.close();
    }

    @FXML
    protected void onSendMessageClick() {
        client = ClientFactory.createInstance(this::addMessage, HOST, SERVER_PORT);
        client.send(message.getText());
        message.clear();
    }

    private void addMessage(String message) {
        textArea.appendText(message + "\n");
    }

    @FXML
    void initialize() {
        executor.submit(() -> {
            server = SimpleServer.create(this::addMessage, SERVER_PORT);
            server.start();
        });
    }
}