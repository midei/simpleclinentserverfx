package ru.kemsu.semestrwork.server;

import java.io.*;
import java.net.Socket;

 class ClientTask implements Runnable {
    private final Socket client;

     private ClientTask(Socket client) {
        this.client = client;
    }

    static ClientTask createTask(Socket client) {
        return new ClientTask(client) ;
    }

    @Override
    public void run() {
        try {
            DataOutputStream outputStream = new DataOutputStream(client.getOutputStream());
            DataInputStream inputStream = new DataInputStream(client.getInputStream());
            while (!client.isClosed()) {
                String entry = inputStream.readUTF();
                outputStream.writeUTF("Server reply - " + entry + " - OK");
                outputStream.flush();
            }
            closeConnections(outputStream, inputStream, client);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeConnections(DataOutputStream outputStream, DataInputStream inputStream, Socket client) throws IOException {
        outputStream.close();
        inputStream.close();
        client.close();
    }
}
